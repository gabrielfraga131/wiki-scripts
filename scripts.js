/* Códigos JavaScript aqui colocados serão carregados por todos aqueles que acessarem alguma página deste wiki */

/*mw.loader.load( 'http://localhost:10001/index.js' );*/



/*Puglins */

$.fn.isInViewport = function () {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height() - 500;
    return elementBottom > viewportTop && elementTop < viewportBottom;
};

$.fn.toggleSetasTOC = function (remov, add) {
    this.removeClass(remov)
    this.addClass(add)
    return this
}

$.fn.expandir = function () {
    this.css({
        display: "block",
        overflow: "hidden"
    })
}

$.fn.versaoNaoLancada = function (data) {
    var lancamento = formatarData(data)
    var hoje = new Date()

    if (lancamento >= hoje) {
        $(this).hide()
    } else {
        $(this).show()
    }
}

    /*plugin para capturar todas as classes de um elemento*/
    ; !(function ($) {
        $.fn.classes = function (callback) {
            var classes = [];
            $.each(this, function (i, v) {
                var splitClassName = v.className.split(/\s+/);
                for (var j = 0; j < splitClassName.length; j++) {
                    var className = splitClassName[j];
                    if (-1 === classes.indexOf(className)) {
                        classes.push(className);
                    }
                }
            });
            if ('function' === typeof callback) {
                for (var i in classes) {
                    callback(classes[i]);
                }
            }
            return classes;
        };
    })(jQuery);

/**Plugins*/

$('head').append(
    '<meta name="viewport" content="width=device-width,initial-scale=1">',
    '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">'
);

$(document).ready(function () {
    organizarPaginas()
    personalizarElementos();
})

const offsetAnchor = function () {
    window.scrollTo(window.scrollX, window.scrollY - 75);
}

const hashClicado = function (hash) {

    document.querySelectorAll('.titulo-menu span').forEach(function (link) {

        if (hash === "#" + link.id) {
            colapsavel($(link).closest('.mw-collapsible-toggle'), $(link).closest('.mw-collapsible-toggle').parent('div'))
        }
    })
}

$(window).load(function () {
    hashClicado(window.location.hash)
    window.setTimeout(offsetAnchor, 0)
    $("#loader").fadeOut("slow");
})

$(document).on('click', 'a[href*="#"]', function (e) {
    hashClicado(this.hash)
    window.setTimeout(offsetAnchor, 0)
})

const personalizarElementos = function () { $(window).width() > 992 ? paginaDesktop() : paginaMobile() }

const urlRetorno = function(){

    if (window.location.href.length > 31){
        var retorno
        window.location.href.indexOf('title=') > 0 ? 
        retorno = window.location.href.slice(window.location.href.indexOf('title=')+6)
        : retorno = window.location.href.slice(window.location.href.indexOf('index.php')+10)
        return retorno
    }
    else return 'Página_principal'
    
}

//Disposição de elementos alterada conforme tamanho da tela
$(window).on('resize', personalizarElementos);

const organizarPaginas = function () {
    $('#mw-navigation').prependTo('body')
    $('<div id="content-container">').insertBefore('#footer')
    $('#content-container').append($('#mw-panel'), $('#content'))

    //Remoção de elementos desnecessários
    $('#p-personal, #left-navigation, #p-logo, #mw-page-base, #mw-head-base, #p-navigation, #p-tb, #ca-nstab-special, #contentSub, #ca-view, #wpSummaryLabel, #wpSummary, .editCheckboxes').remove();
}

const paginaDesktop = function () {
    exibirNomePaginaDesktop()

    if (!$('#indiceResponsivo').length) {
        $('#content').attr('style', 'width: 100%')
        $('#mw-panel, #anchor').remove()

    }
    else {
        $('#content, #mw-painel').attr('style', '');
        adicionarAncora()
    }
}

const exibirNomePaginaDesktop = function () {
    $('#bodyContent').before($('#firstHeading'))
    $('#titulo-etapas').length ? $('#firstHeading').hide() : false

}
const adicionarAncora = function () {

    $('<i id="anchor" data-top="#top" class="noprint fa fa-arrow-up">')
        .insertBefore('#siteSub')

    //Exibir
    $(window).on('resize scroll', function () {
        if (window.scrollY >= 1000) {
            $('#anchor').fadeIn("slow")
        }
        else if (window.scrollY < 1000) {
            $('#anchor').fadeOut("slow")
        }
    })

    //Clicar
    $('[data-top]').on('click', function () {
        window.location.hash === $(this).data('top') ? window.location.hash = ""
            : window.location.hash = $(this).data('top')

    })
}

const paginaMobile = function () {
    exibirNomePaginaMobile()
    $('#anchor').remove()
    $('.menu-back-office').expandir()
}

const exibirNomePaginaMobile = function () {
    $('#p-search').before($('#firstHeading'))
    $('#titulo-etapas').length ? $('#firstHeading').show() : false
}


const popularMenuNavegacao = function () {

    const listaMenuSideBar = [
        { id: 'n-Pagina-Inicial', url: 'P%C3%A1gina_principal', label: 'Página Principal', icon: 'home-icon', i: 'hide' },
        { id: 'n-Etapas-da-Compra', url: 'Etapas_da_Compra', label: 'Etapas da compra', icon: 'compras-icon', i: 'hide' },
        { id: 'n-Back-Office', url: 'Back-Office', label: 'Back-Office', icon: 'backoffice-icon', i: 'hide' },
        { id: 'n-Indicadores', url: 'Indicadores', label: 'Indicadores', icon: 'indicadores-icon', i: 'hide' },
        { id: 'n-Aplicativos', url: 'Aplicativos', label: 'Aplicativos', icon: 'aplicativos-icon', i: 'hide' },
        { id: 'n-Tutoriais', url: 'Tutoriais', label: 'Tutoriais', icon: 'tutoriais-icon', i: 'hide' },
        { id: 'n-Notas', url: 'Versoes', label: 'Versões', icon: 'notas-icon', i: 'fa fa-wrench' },

        { id: 'pt-preferences', url: 'Especial:Prefer%C3%AAncias', label: 'Preferências', icon: 'preferencias-icon', i: 'fa fa-check-square' },
        { id: 't-upload', url: 'Especial:Carregar_arquivo', label: 'Enviar arquivo', icon: 'enviar-arquivo-icon', i: 'fa fa-cloud-upload' },
        { id: 't-specialpages', url: 'Especial:P%C3%A1ginas_especiais', label: 'Páginas especiais', icon: 'especial-icon', i: 'fa fa-fw fa-asterisk' },
        { id: 'pt-logout', url: ' ', label: 'Sair', icon: 'logout-icon', i: 'fa fa-fw fa-sign-out' }
    ]

    var liMenu = listaMenuSideBar.map(function (lista) {

        return $('<li id="' + lista.id + '">').append(

            $('<a class="menu-item" href= http://wiki.vipcommerce.com.br/index.php/' + lista.url + ' >')
                .append($('<span class="' + lista.icon + ' lateral-menu-icon"></span>')
                    .append('<i class="' + lista.i + '">'))
                .append(lista.label)
        )

    })

    return liMenu;
}

/*Este  tratamento é inverso dado a limitações de edição.  O JS não pode editar a tela 
de login. Portanto, por padrão alguns itens aparecem apenas onde o Js pode atuar. Dessa forma, na tela de login nenhum item desnecessário é exibido*/

const personalizarPorTipoUsuario = function () {
    if (wgUserName !== null) {
        $('#mw-panel').show();
        $('#right-navigation, .nav').attr('style', 'display: flex;')

        if (wgUserName === "Vipcommerce") {
            $('#ca-history,#p-cactions, #ca-view, #pt-preferences, #t-upload, #t-specialpages, .admin-only').hide();
            $('.dropdown-toggle').text('Olá: cliente VIPCommerce')
        }
        else {
            $('.dropdown-toggle').text('Olá: ' + wgUserName)
        }
        $('a.dropdown-toggle').append('<span class="caret"></span>')
    }
    else {
        $('#menu-navegacao, #banner-versao, #footer, label.menu-icon').hide()

        $('#mw-content-text').html($('<p>É necessário autenticar-se para acessar a Ajuda VIPCommerce. Para isso, basta acessar o Back-Office e clicar em <strong>Ajuda</strong> no canto superior direito da tela. Após isso, esta página pode ser recarregada e outros links da Ajuda poderão ser acessados.<br><br><img src="/images/3/38/Autentique-se.png"><br><br>Caso faça parte da equipe VIPCommerce e possua um login na Ajuda, <a href="/index.php?title=Especial:Autenticar-se&returnto='+urlRetorno()+'">autentique-se aqui.</a></p>'))

    }

}

//Verificar
const logout = function () {
    $('#pt-logout a').attr("href", "http://wiki.vipcommerce.com.br/index.php?title=Especial:Sair&returnto=" + urlRetorno())
}


$(function telasComEtapas() {

    const exibirItem = function (item) {
        $('.item-renderizavel').hide()
        $('#' + item).show()
    }
    const trocarCorColuna = function (col) {

        $('.SubPagina').removeClass('ColunaAbertaSubPaginas')
        $(col).addClass('ColunaAbertaSubPaginas')

    }
    $('.item-renderizavel').each(function () {
        if (window.location.href.indexOf(this.id) > 0) {
            trocarCorColuna($('[data-slug=' + this.id + ']'))
            exibirItem(this.id)
        }
    })
    $('.SubPagina').on('click', (function (e) {
        e.preventDefault()
        trocarCorColuna(this)
        exibirItem($(this).data('slug'))

    }))

})

//Essa função transforma o toc em uma ferramenta mais útil ao usuário. 

$(function TOCLateral() {

    //Índice sai da página e entra no painel

    $("#indiceResponsivo").appendTo("#mw-panel");

    //Insere classes de recursos visuais ao toc

    $('#toc ul li ul').siblings('a').addClass('titulo-toc')   //Aplica a classe somente quando há filho 
    $('.titulo-toc ~ul').addClass("lista-escondida");         //Aplica classe às listas 
    $('.titulo-toc').append('<i class="fa fa-angle-down">')   //Aplica ícone de seta às listas  


    //Atualizará o toc conforme scroll

    const atualizarTOC = function () {

        $('.toc a:not(.active)')
            .parents('.lista-escondida')
            .removeClass('open-menu')

        $('.toc .active')
            .parents('.lista-escondida')
            .addClass('open-menu')


        $('.lista-escondida')
            .filter(':hidden')
            .siblings('a')
            .removeClass('active-menu')
            .children('i')
            .removeClass('fa-angle-up')
            .addClass('fa-angle-down')

        $('.lista-escondida')
            .filter(':visible')
            .siblings('a')
            .addClass('active-menu')
            .children('i')
            .removeClass('fa-angle-down')
            .addClass('fa-angle-up')

    }

    $(window).on('resize scroll', function () {

        atualizarTOC()

        var elementos = $('.mw-headline');
        var menu = $('.toc a')

        $(elementos).each(function () {
            var elm = "#" + this.id

            if ($(this).isInViewport()) {
                var dismatch = $.grep(menu, function (e) { return e.hash !== elm; })
                $(dismatch).removeClass('active')

                var match = $.grep(menu, function (e) { return e.hash === elm; })
                $(match).addClass('active')

            }

        })

        if ($('#indiceResponsivo').length && document.querySelector('.active') != null) {
            document.querySelector('.active').scrollIntoView({
                behavior: 'smooth'
            });
        }

    })

    /*versão para impressão */
    /*
        window.onbeforeprint = function () {
            $("#indiceResponsivo").appendTo("#printIndice");
            $('#indiceResponsivo').addClass('indice-on-print')
        };
        window.onafterprint = function () {
            $("#indiceResponsivo").appendTo("#mw-panel");
            $('#indiceResponsivo').removeClass('indice-on-print')
        };*/
})


$(function criarBarraNavegacao() {

    /*Elementos adicionados barra de navegação*/


    //Desktop
    $('#mw-head').prepend('<div class="container">')

    const navBar = $('<div id="menu-navegacao" class="nav navbar-nav navbar-left">')
    const navUl = $('<ul class="nav navbar-nav hidden-mobile principal-nav">')

    $(navUl).append([
        '<li><a class="item-barra-navegacao" href="/"><img id="logo" class="no-detail" src="/vip-commerce.png" width="51" height="32"></a></li>',
        '<li><a class="item-barra-navegacao" href="/">Ajuda</a></li>',
        '<li><a class="item-barra-navegacao" href="#"><i class="fa fa-fw fa-phone" aria-hidden="true"></i> +55 (31) 4141-0305</a></li>',
        '<li><a class="item-barra-navegacao" href="mailto:suporte@vipcommerce.com.br"><i class="fa fa-fw fa-address-card-o" aria-hidden="true"></i>&nbsp;suporte@vipcommerce.com.br</a></li>',
        '<li class="dropdown"><a class="dropdown-toggle item-barra-navegacao" data-toggle="dropdown"></a><ul id="menu-topo" class="dropdown-menu"></ul></li>'

    ])

    $('.container').prepend(navBar.append(navUl))

    $('.container').append([
        '<input class="hidden-desktop" type="checkbox" id="chk">',
        '<label for="chk" class="menu-icon menu-estilo hidden-desktop" >&#9776; </label>',
        '<div class="bg hidden-desktop"></div>',
        '<div class="menu hidden-desktop" id="principal"></div>'
    ])


    //Mobile

    $('#principal').append([
        '<div class="cabecalho menu-estilo hidden-desktop">Navegação</div>',
        $('<ul class="lista-mobile"></ul>')
    ])

    $('.cabecalho').append('<i class="menu-estilo hidden-desktop voltar fa fa-close">');


    $('#right-navigation').append('<i class="menu-estilo fa hidden-desktop fa-search">')
    $('#right-navigation').appendTo('.container')
    $('#p-views').find('a').addClass('item-barra-navegacao')

    //Verificar
    const fecharMenuTopo = function (menu) {
        $('.dropdown-toggle').toggleClass('selected')
        $(menu).toggle()
    }

    $('.mw-body').on('click', function () {
        document.querySelectorAll('.dropdown-menu').forEach(function (menu) {
            $(menu).is(":visible") ? fecharMenuTopo(menu) : false

        })
    });

    $('.dropdown-toggle').on('click', function () {
        $(this).siblings('ul').toggle()
        $(this).toggleClass('selected')
    })

    const fechaMenuLateral = function () {
        $('#chk').prop('checked', false)
    }

    $('.voltar, .bg').on('click', fechaMenuLateral)

    $('.lista-mobile, #menu-topo').html(popularMenuNavegacao)
    personalizarPorTipoUsuario()
    logout()

})


/*Barra de pesquisa responsiva */

$(function barraDePesquisaInterativa() {

    $('#simpleSearch').append('<i class="hidden-desktop fechaPesquisa fa fa-close"></i>');

    const toggleBarraPesquisa = function () {
        if (!$('#p-search').hasClass('open')) {
            $('#p-search').addClass('open');
            $('#firstHeading, .fa-search').hide();
            $('#searchInput').focus();
        }
        else {
            $('#p-search').removeClass('open');
            $('#firstHeading, .fa-search').show();
        }
    }

    $('.fechaPesquisa, .fa-search').on('click', toggleBarraPesquisa);

    //Barra de pesquisa se adapta ao flex-box. Organizando os elementos na ordem necessária
    $('#simpleSearch').prepend($('#searchButton'))

})


/*firstHeading é o título da página. É feito um tratamento para o mesmo
ficar fixo na barra de navegação na versão responsiva*/

$('#firstHeading').addClass("menu-estilo");

$('#wikiPreview, #mw-clearyourcache').addClass("hide-banner");

//Esconde titulo em páginas de processo, pois há um Heading personalizado nas mesma

$('#titulo-etapas').length ? $('#firstHeading').hide() : $('#firstHeading').show()



//"Embeda" o vídeo tutorial na wiki

$('#video').append('<iframe width="560" height="315" src="https://www.youtube.com/embed/DuWLGibRmaU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>')


const colapsavel = function (menu, seta) {
    $(seta).toggleClass('mw-collapsed mw-expanded')
    // $(menu).toggleClass('mw-collapsible-toggle-expanded mw-collapsible-toggle-collapsed')
    $(menu).siblings('.mw-collapsible-content').expandir()
}


//Novo Footer
$(function () {

    $('#footer-info, #footer-places, #footer-icons').remove()

    const footerEsquerda = $('<div class="fundo-footer esquerdo">')
        .append($('<div class="texto-footer">')
            .append($('<span class="copyright">© Todos os direitos reservados VIPCommerce</span>')))

    const footerDireita = $('<div class="fundo-footer direito">')
        .append($('<div class="texto-footer">')
            .append($('<span class="copyright">http://www.vipcommerce.com.br</span>')
            ))
    $('#footer').append(footerEsquerda, footerDireita)
})

//Modal de detalhe de imagens
$(function () {

    const modal = $('<div id="modal" class="wiki-modal">')
        .append($('<span class="close">&times;</span>'))

        .append($('<a id="view-detail">')
            .append($('<i class="fa fa-info">')))

        .append($('<div id="img-container">')
            .append($('<img class="modal-content fade-down" id="img01">')))

    $('body').append($(modal))

    //Ação de abrir o modal
    $('img:not(.no-detail)').on('click', function (e) {

        if ($("#indiceResponsivo").length ||
            $('#toc-indicadores').length) {
            e.preventDefault()

            var urlDetalhe = $(this).parent('a').attr("href")
            $('#view-detail').attr({ 'href': urlDetalhe, 'target': '_blank' })
            $('#modal').attr('style', 'display: flex;')

            if (this.src.search("thumb") >= 0) {

                var indexThumb = this.src.indexOf("thumb")
                var nomeArquivo = urlDetalhe.slice((urlDetalhe.indexOf("Arquivo:") + 8))
                var indexNomeArquivo = this.src.indexOf(nomeArquivo)

                var stringFinal = this.src.slice(indexThumb, indexNomeArquivo)
                stringFinal = stringFinal
                    .replace("thumb", "/images")
                    .concat(nomeArquivo)

                $('#img01').attr("src", stringFinal)

            } else {
                $('#img01').attr("src", this.src)

            }

        }
    })

    //Ação que fecha o Modal
    // adicionar uma classe que irá aplicar a animação e fechar o modal após

    const fecharModalImagens = function () {
        $('#img01').animate({
            marginTop: "-250px",
            opacity: '0'
        }, 300
            , function () {
                $('#modal').hide()
            })
        $('#img01').animate({
            marginTop: "0",
            opacity: '1'
        })

    }
    $(document).on('keydown', function (e) {
        if ((e.keyCode === 27) && ($("#modal").length))
            fecharModalImagens()
    })

    $('.close, #modal').on('click', fecharModalImagens)

    //Não executa nada ao clicar na imagem
    $('#img01').on('click', function () {
        return false
    })

})

$('.status-compras').each(function () {
    var campo = $('<a href="/index.php/Status_das_Compras" title="Clique para acessar a documentação de status de compra">')
    $(campo).insertBefore(this)
    $(this).appendTo(campo)

})

$('[data-vp-url]').on('click', function () {
    window.location = $(this).find('a').attr('href')
})

/*Modal de aviso de versão nova*/

const inserirBanner = function () {

    var banner = $('<div id="banner-versao" class="noprint">')
    var a = $('<a class="banner-link "href="Notas de Versão">')
    var span = $('<span> Versão 1.21 do sistema publicada em 04 de Junho. Confira o que há de novo clicando aqui.</span>')
    var close = $('<i class="fechar fa fa-close">')

    return banner.append(a.append(span)).append(close)
}
/*
$(function () {
    if ((!$('.hide-banner').length) && (wgUserName !== null)) {

        $('#content').prepend(inserirBanner())
        $('.fechar').on('click', fecharPai)

    }
})*/

const fecharPai = function () {
    $(this).parent().hide()
}

const formatarData = function (data) {
    var ndata = data.split('/').concat()
    var novaData = new Date(ndata[2], ndata[1], ndata[0])
    novaData.setMonth(novaData.getMonth() - 1)
    return novaData
}

$('[data-versao="1.21"]').versaoNaoLancada('03/06/2019')

$(function botoesWiki() {

    $('#wpSave').addClass('btn-success')
    $('#wpPreview, .historysubmit').addClass('btn-warning')
    $('#wpDiff').addClass('btn-info')
    $('input[value="Ver"], input[value="Pesquisar"]').addClass('btn-primary')

    $('.cancelLink').html('<a id="mw-editform-cancel" href="/index.php/' + wgPageName + '" title="' + wgPageName + '" >Cancelar</a>')
})

$('[data-new-page]').children('a').attr("target", "_blank")

